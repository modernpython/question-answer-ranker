Question Answer Ranker

by

Arjun Malik        14075060
Manish Kumar Singh 14075065

under supervision of:Prof. A. K. Singh

Description:
1)Run the GBRank.py file.

2)By default it runs on dataTrec99.txt(Data of TREC Evaluation 1999)

3)This can be modified by changing the line 57 

	from

fea=process_doc("dataTrec99.txt")

	to

fea=process_doc(Name Of Data File in String)

4)The function_h function can be used to calculate score of any question answer tuple 
in order to compare it with some other tuple corresponding to same query.

5)clfvec is a part of the above function definition and once it is learnt it can be used to score any given feature vector. 

6)After running GBrank.py file the clfvec list can be input to function_h to get the final ranking function.